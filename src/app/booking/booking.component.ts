import { Component, OnInit } from '@angular/core';
import { BookingService } from './booking-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SecurityService } from '../security-service.service';

@Component({
  selector: 'app-booking', 
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})

export class BookingComponent implements OnInit{

  _bookings: Array<any> = new Array();
  _filterOption: string;
  _filterValue: number;
  _field: string;

  
  constructor(private _bookingService: BookingService, 
    private _router: Router, 
    private _activatedRoute: ActivatedRoute,
    private _securityService: SecurityService){

  }

  ngOnInit(){

    if(!this._securityService.getToken()){
      this._router.navigate(['/login'])
    }else{

      this._bookingService.getBookings('contacto@tuten.cl').subscribe(
        (data) => {
          for(var i = 0; i < data.length; i++){
            data[i].bookingFields = JSON.parse(data[i].bookingFields);
          }
          this._bookings = data;
        }, (error) => {
        }
      )

    }

    
  }

}