import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { UrlService } from '../url-service.service';
import { SecurityService } from '../security-service.service';
import { Observable } from 'rxjs';

@Injectable()

export class BookingService{

  private _tokenHeader: string = 'Token';
  
  constructor(private _http: HttpClient, 
    private _urlService: UrlService, 
    private _securityService: SecurityService){

  }

  getBookings(userEmail: string): Observable<any>{
    let params = new HttpParams().set('current', 'true'); 
    let headers: HttpHeaders = this._urlService._basicHeaders.append(this._tokenHeader, this._securityService.getToken());
    headers = headers.append('Adminemail', 'testapis@tuten.cl');
    return this._http.get(this._urlService._basePath + '/user' + `/${userEmail}` + '/bookings',{headers: headers, params: params})


  }

}