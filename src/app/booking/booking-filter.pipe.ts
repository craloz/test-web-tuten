import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bookingFilter',
  pure: false
})

export class BookingFilterPipe implements PipeTransform{

  transform(value: any, filterValue: string, operator: string, field: string): any{


    if (value.length === 0 || !filterValue || !operator || !field) {
      return value;
    }

    const resultArray = [];

    if(operator === 'like'){

      for(var i = 0; i< value.length;i++){
        if(value[i][field] === filterValue){
          resultArray.push(value[i])
        }
      }
      

    } else if(operator === 'greater'){

      for(var i = 0; i< value.length;i++){
        if(filterValue <= value[i][field]){
          resultArray.push(value[i])
        }
      }

    } else {

      for(var i = 0; i< value.length;i++){
        if(filterValue >= value[i][field]){
          resultArray.push(value[i])
        }
      }

    }

    return resultArray;
  
  }


}