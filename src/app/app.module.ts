import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UrlService } from './url-service.service';
import { SecurityService } from './security-service.service';
import { LoginService } from './login/login-service.service';
import { BookingComponent } from './booking/booking.component';
import { BookingService } from './booking/booking-service.service';
import { BookingFilterPipe } from './booking/booking-filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BookingComponent,
    BookingFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    UrlService, 
    LoginService,
    SecurityService, 
    BookingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
