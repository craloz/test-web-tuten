import { Injectable } from '@angular/core';
import { UrlService } from '../url-service.service';
import { SecurityService } from '../security-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class LoginService{

  private _passwordHeader:string = 'Password';

  constructor(private _http: HttpClient, private _urlService: UrlService, private _securityService: SecurityService){

  }  

  login(user: any): Observable<any>{
    let subject = new Subject();
    let headers: HttpHeaders = this._urlService._basicHeaders.append(this._passwordHeader, user.password);
    this._http.put(this._urlService._basePath + '/user' + `/${user.email}`, {},{headers: headers}).subscribe(
      (data: any) => {
        this._securityService.saveToken(data.sessionTokenBck);
        subject.next(data);
      }, (error) => {
        subject.error(error);
      }
    );
    return subject.asObservable();
    
  }

}