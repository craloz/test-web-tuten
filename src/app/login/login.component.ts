import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginService } from './login-service.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent{

  user = 'testapis@tuten.cl';
  password = '1234';

  constructor(private _loginService: LoginService, 
    private _router: Router){
  }


  onLogin(loginForm: NgForm){
    this._loginService.login(loginForm.value).subscribe(
      (data) => {
        this._router.navigate(['/booking'])
      }, (error) => {
        alert('Credenciales invalidas')
      }
    )
  }
}