export class SecurityService{
  
  private _token:string;
  
  constructor(){

  }

  saveToken(token: string){
    this._token = token;
  }

  getToken(){
    return this._token;
  }

}