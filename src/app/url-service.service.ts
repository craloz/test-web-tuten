import { HttpHeaders } from '@angular/common/http';

export class UrlService{

  _basePath: string = 'https://dev.tuten.cl:443/TutenREST/rest';

  _basicHeaders: HttpHeaders = new HttpHeaders();;


  constructor(){

    this._basicHeaders = this._basicHeaders.append('App', 'APP_BCK');

  }


}